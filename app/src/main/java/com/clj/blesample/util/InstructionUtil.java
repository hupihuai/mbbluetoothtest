package com.clj.blesample.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * created by hupihuai on 2020-01-02
 */
public class InstructionUtil {

    public static String toHexString(byte[] byteArray, String divide) {
        final StringBuilder hexString = new StringBuilder("");

        if (byteArray == null || byteArray.length <= 0)
            return null;
        int length = byteArray.length;
        for (int i = 0; i < length; i++) {
            int v = byteArray[i] & 0xFF;
            String hv = Integer.toHexString(v);
            hexString.append("0x");
            if (hv.length() < 2) {
                hexString.append(0);
            }
            hexString.append(hv);
            if (i != length - 1) {
                hexString.append(divide);
            }
        }
        return hexString.toString().toLowerCase().trim();
    }


    public static byte calcFrameCheckSum(byte[] data) {
        return calcFrameCheckSum(data, 0, data.length);
    }

    public static byte calcFrameCheckSum(byte[] data, int start, int length) {
        byte sum = 0;
        if (start + length > data.length) {
            return sum;
        }
        for (int i = start; i < start + length; ++i) {
            sum += data[i];
        }
        sum &= 0xff;
        return sum;
    }


    public static byte[] getdataDataWithMsgID(int msgId) {

        byte prefix = (byte) 0xf3;
        byte suffix = (byte) 0xf4;
        byte protocolId = 0x28;
        ByteBuffer dataValue = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
        dataValue.put((byte) 0x01);
        dataValue.put((byte) (msgId & 0xFF));
        dataValue.put((byte) (msgId >> 8 & 0xFF));
        int dataLength = dataValue.array().length + 1;
        byte comandLengthLow = (byte) (dataLength & 0xff);
        byte commandLengthHigh = (byte) (dataLength >> 8 & 0xFF);
        //head check
        int intCheck = (int) prefix + (int) comandLengthLow + (int) commandLengthHigh;

        int sumData = protocolId;
        byte[] array = dataValue.array();
        int length = array.length;
        for (int i = 0; i < length; i++) {
            sumData += (int) array[i];
        }

        byte[] dataRealArray = new byte[]{prefix,
                (byte) (intCheck & 0xff),
                comandLengthLow,
                commandLengthHigh,
                protocolId};

        ByteBuffer dataRealValue = ByteBuffer.allocate(10).order(ByteOrder.LITTLE_ENDIAN);

        dataRealValue.put(dataRealArray);
        dataRealValue.put(array);
        dataRealValue.put((byte) (sumData & 0xff));
        dataRealValue.put(suffix);
        return dataRealValue.array();


    }


}
