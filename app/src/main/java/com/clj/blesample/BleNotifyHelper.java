package com.clj.blesample;

import com.clj.blesample.util.InstructionUtil;
import com.clj.fastble.BleManager;
import com.clj.fastble.callback.BleNotifyCallback;
import com.clj.fastble.data.BleDevice;
import com.clj.fastble.exception.BleException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * created by hupihuai on 2020-03-13
 */
public class BleNotifyHelper {

    public static BleNotifyHelper getInstance() {
        return InnerInstance.bleNotifyHelper;
    }

    private static class InnerInstance {
        public static BleNotifyHelper bleNotifyHelper = new BleNotifyHelper();
    }

    private List<NotifyCallback> notifyCallbackList = new ArrayList<>();


    public void registNotify(final BleDevice bleDevice) {
        BleManager.getInstance()
                .notify(bleDevice, MainActivity.uuid_service, MainActivity.uuid_notify, new BleNotifyCallback() {
                    @Override
                    public void onNotifySuccess() {
                    }

                    @Override
                    public void onNotifyFailure(BleException exception) {

                    }

                    @Override
                    public void onCharacteristicChanged(byte[] data) {
//                        String instruction = InstructionUtil.toHexString(data, ",");
//                        System.out.println("response = " + instruction);
                        notifyCallback(bleDevice, data);
                    }
                });
    }


    private void notifyCallback(BleDevice bleDevice, final byte[] data) {
        int size = notifyCallbackList.size();
        for (int i = 0; i < size; i++) {
            notifyCallbackList.get(i).onCharacteristicChanged(bleDevice, data);
        }
    }

    public interface NotifyCallback {
        void onCharacteristicChanged(BleDevice bleDevice, byte[] data);
    }

    public void addNotifyCallback(NotifyCallback notifyCallback) {
        notifyCallbackList.add(notifyCallback);
    }

    public void removeNotifyCallback(NotifyCallback notifyCallback) {
        notifyCallbackList.remove(notifyCallback);
    }

}
