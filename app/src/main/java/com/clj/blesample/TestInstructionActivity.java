package com.clj.blesample;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.clj.blesample.data.SendItemData;
import com.clj.blesample.data.StatisticsData;
import com.clj.blesample.util.InstructionUtil;
import com.clj.fastble.BleManager;
import com.clj.fastble.callback.BleWriteCallback;
import com.clj.fastble.data.BleDevice;
import com.clj.fastble.exception.BleException;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * created by hupihuai on 2020-03-12
 */
public class TestInstructionActivity extends AppCompatActivity {


    public Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                String txt = msg.getData().getString("log");
                if (consoleTv != null) {
                    if (consoleTv.getText() != null) {
                        if (consoleTv.getText().toString().length() > 5000) {
                            consoleTv.setText("日志定时清理完成..." + "\n\n" + txt);
                        } else {
                            consoleTv.setText(consoleTv.getText().toString() + "\n\n" + txt);
                        }
                    } else {
                        consoleTv.setText(txt);
                    }
                    scrollView.post(new Runnable() {
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                }
            }
            super.handleMessage(msg);
        }

    };

    private TextView consoleTv;
    private ScrollView scrollView;
    private EditText sendCountEt;
    private EditText sendDurationEt;

    private SparseArray<SendItemData> sendItemDataList = new SparseArray<>();
    private Map<String, StatisticsData> statisticsDataMap = new HashMap<>();

    private BleNotifyHelper.NotifyCallback notifyCallback = new BleNotifyHelper.NotifyCallback() {
        @Override
        public void onCharacteristicChanged(BleDevice bleDevice, byte[] data) {
            if (data[0] == (byte) 0xf3) {
                byte[] dataArray = new byte[]{data[6], data[7]};
                int resultIndex = byte2int(dataArray, 2);
                System.out.println("resultIndex = " + resultIndex);
                sendmsg("收到-" + bleDevice.getName() + "-" + InstructionUtil.toHexString(data, ","));
                SendItemData sendItemData = sendItemDataList.get(resultIndex);
                if (sendItemData != null) {
                    sendItemData.responseTime = System.currentTimeMillis();
                }
            }
        }
    };

    public static int byte2int(byte[] data, int len) {
        int res = 0;
        switch (len) {
            case 1: {
                res = data[0] & 0xff;
                return res;
            }
            case 2: {
                res = data[0] & 0xff | (data[1] & 0xff) << 8;
                return res;
            }
            case 3: {
                res = data[0] & 0xff | (data[1] & 0xff) << 8 | (data[2] & 0xff) << 16;
                return res;
            }
            case 4: {
                res = data[0] & 0xff | (data[1] & 0xff) << 8 | (data[2] & 0xff) << 16 | (data[3] & 0xff) << 24;
                return res;
            }
            default:
                return res;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        initView();
        setViewListener();

        BleNotifyHelper.getInstance().addNotifyCallback(notifyCallback);
    }

    private void initView() {
        consoleTv = findViewById(R.id.consoleTv);
        scrollView = findViewById(R.id.scrollView);
        sendCountEt = findViewById(R.id.sendCountEt);
        sendDurationEt = findViewById(R.id.sendDurationEt);
    }

    private void setViewListener() {
        findViewById(R.id.sendBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItemDataList.clear();
                statisticsDataMap.clear();

                String sendCountStr = sendCountEt.getText().toString();
                if (TextUtils.isEmpty(sendCountStr)) {
                    Toast.makeText(TestInstructionActivity.this, "次数不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }

                String sendDurationStr = sendDurationEt.getText().toString();
                if (TextUtils.isEmpty(sendDurationStr)) {
                    Toast.makeText(TestInstructionActivity.this, "时间不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }

                final int sendCount = Integer.parseInt(sendCountStr);
                final long sendDuration = Long.parseLong(sendDurationStr);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int index = 0;
                        for (int i = 0; i < sendCount; i++) {
                            List<BleDevice> allConnectedDevice = BleManager.getInstance().getAllConnectedDevice();
                            int size = allConnectedDevice.size();
                            for (int j = 0; j < size; j++) {
                                byte[] bytes = InstructionUtil.getdataDataWithMsgID(index);
                                System.out.println("sendIndex = " + index);
                                sendmsg("发送-" + index + " " + InstructionUtil.toHexString(bytes, ","));
                                sendRunCommand(allConnectedDevice.get(j), bytes, index);
                                index++;
                            }
                            try {
                                Thread.sleep(sendDuration);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        //完成统计
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                int size = sendItemDataList.size();
                                if (size > 0) {
                                    for (int j = 0; j < size; j++) {
                                        SendItemData sendItemData = sendItemDataList.get(j);
                                        StatisticsData statisticsData = getStatisticsData(sendItemData.bleDevice);
                                        long destWriteTime = sendItemData.sendSuccessTime - sendItemData.sendTime;
                                        long destReponseTime = 0;
                                        if (sendItemData.responseTime > 0) {
                                            destReponseTime = sendItemData.responseTime - sendItemData.sendTime;
                                            statisticsData.addSuccessCount(1);
                                        } else {
                                            statisticsData.addFailCount(1);
                                        }
                                        statisticsData.addOneResponseDestTime(destReponseTime);
                                        statisticsData.addWriteDestTime(destWriteTime);
                                    }

                                    Set<String> keySet = statisticsDataMap.keySet();
                                    Iterator<String> iterator = keySet.iterator();
                                    while (iterator.hasNext()) {
                                        String key = iterator.next();
                                        StatisticsData statisticsData = statisticsDataMap.get(key);
                                        String result = statisticsData.toString();
                                        sendmsg(result);
                                    }
                                }
                            }
                        }, 3000);
                    }
                }).start();

            }
        });
    }

    private StatisticsData getStatisticsData(BleDevice bleDevice) {
        StatisticsData statisticsData = statisticsDataMap.get(bleDevice.getName());
        if (statisticsData == null) {
            statisticsData = new StatisticsData();
            statisticsData.bleDevice = bleDevice;
            statisticsDataMap.put(bleDevice.getName(), statisticsData);
        }
        return statisticsData;
    }

    private byte hexToByte(String inHex) {
        inHex = inHex.replace("0x", "");
        return (byte) Integer.parseInt(inHex, 16);
    }

    private void sendmsg(String txt) {
        Message msg = new Message();
        msg.what = 1;
        Bundle data = new Bundle();
        long l = System.currentTimeMillis();
        Date date = new Date(l);
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss:SSS");
        String d = dateFormat.format(date);
        data.putString("log", d + ":" + "" + txt);
        msg.setData(data);
        try {
            handler.sendMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendRunCommand(final BleDevice bleDevice, byte[] data, int index) {
        final long startMillis = System.currentTimeMillis();
        final SendItemData sendItemData = new SendItemData();
        sendItemDataList.put(index, sendItemData);
        sendItemData.bleDevice = bleDevice;
        sendItemData.sendTime = startMillis;
        BleManager.getInstance().write(bleDevice, MainActivity.uuid_service, MainActivity.uuid_write, data, new BleWriteCallback() {
            @Override
            public void onWriteSuccess(int current, int total, byte[] justWrite) {
                long destTime = System.currentTimeMillis() - startMillis;
                sendItemData.sendSuccessTime = System.currentTimeMillis();
                sendmsg("发送完成-时间：" + bleDevice.getName() + "-destTime=" + destTime + "-" + InstructionUtil.toHexString(justWrite, ","));
            }

            @Override
            public void onWriteFailure(BleException exception) {
                System.out.println("exception = " + exception.getDescription());
                sendmsg("发送失败-" + bleDevice.getName());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BleNotifyHelper.getInstance().removeNotifyCallback(notifyCallback);
    }
}
