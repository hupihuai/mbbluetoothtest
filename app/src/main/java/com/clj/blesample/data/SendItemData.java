package com.clj.blesample.data;

import com.clj.fastble.data.BleDevice;

/**
 * created by hupihuai on 2020-04-01
 */
public class SendItemData {

    public int index;//指令唯一标示
    public BleDevice bleDevice;
    public long sendTime;
    public long sendSuccessTime;
    public long responseTime;
}
