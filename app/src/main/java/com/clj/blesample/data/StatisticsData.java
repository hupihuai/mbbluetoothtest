package com.clj.blesample.data;

import com.clj.fastble.data.BleDevice;

/**
 * created by hupihuai on 2020-04-01
 */
public class StatisticsData {
    public BleDevice bleDevice;
    private long allResponseDestTime;
    private long allWriteDestTime;
    private int successCount;
    private int failCount;


    public void addOneResponseDestTime(long destIime) {
        allResponseDestTime += destIime;
    }

    public void addWriteDestTime(long destIime) {
        allWriteDestTime += destIime;
    }

    public void addSuccessCount(int count) {
        successCount += count;
    }

    public void addFailCount(int count) {
        failCount += count;
    }

    @Override
    public String toString() {
        return bleDevice.getName() + ":平均响应时间=" + allResponseDestTime / successCount +
                ", 平均写入时间=" + allWriteDestTime / (successCount + failCount) +
                ", 成功次数=" + successCount +
                ", 失败次数=" + failCount;
    }
}
